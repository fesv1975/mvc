<?php
class SiteController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionReadme()
    {
        $content = file_get_contents(MVC::base_path() . '/README.md');
        $this->render('readme',['content' => $content]);
    }

    public function actionPhpinfo()
    {
        phpinfo();
    }
}
