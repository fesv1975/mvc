<div class="container mt-5">
    <!-- README.md -->
    <script type="module" src="https://cdn.jsdelivr.net/gh/zerodevx/zero-md@2/dist/zero-md.min.js"></script>
    <zero-md><script type="text/markdown"><?=$content?></script></zero-md>
</div>