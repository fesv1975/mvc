<?php
class MVC
{
    private static $databases = [];
    private static $controller_name = null;
    private static $action_name = null;
    private static $http_method = null;
    private static $config = [];
    private static $module = '/app';
    private static $controller_file = null;


    function main()
    {
        $config = require(__DIR__ . '/../config/main.php');
        $base = self::base_path();
        try {        
            // parse url
            $url = trim(isset($_GET['_r']) ? $_GET['_r'] : @$_SERVER['REQUEST_URI'], '/');
            if ($url === 'index.php') $url = '';
            unset($_GET['_r']);
            unset($_REQUEST['_r']);
        
            $arr_url = explode('/', $url);
            $controller_url = @array_shift($arr_url);

            # carpeta del controlador
            $controller_path = "{$base}/app";
            while ($controller_url and is_dir("{$controller_path}/modules/{$controller_url}")) {
                $controller_path .= "/modules/{$controller_url}";
                $controller_url = @array_shift($arr_url);
            }

            // sanear nombre de controlador: nombre-controlador -> NombreControlador
            $controller_name = str_replace(' ','',ucwords(str_replace('-',' ',$controller_url)));
            $controller_name = $controller_name ? $controller_name : 'site';
            $controller_name = ucfirst($controller_name . 'Controller');

            $controller_file = $controller_path . "/controllers/{$controller_name}.php";
        
            $action_name = array_shift($arr_url);
            $id = null;
            if (is_numeric($action_name)) {
                $id = $action_name;
                $action_name = null;
            } elseif ($action_name === 'id') {
                $action_name = null;
            }
        
            if ($id === null) {
                $id = array_shift($arr_url);
                if ($id === 'id') {
                    $id = array_shift($arr_url);
                }
            }

            //-------- ACCIONES POR DEFECTO -------------
            // metodo de peticion (GET,POST,PUT,DELETE)
            $method = $_SERVER['REQUEST_METHOD'];
            self::$http_method = $method;

            if ($method === 'GET' and $id === null) {
                $action = $action_name ? $action_name : 'index';
            } elseif ($method === 'GET') {
                $action = $action_name ? $action_name : 'view';
            } elseif ($method === 'POST') {
                $action = $action_name ? $action_name : 'create';
            } elseif ($method === 'PUT') {
                $action = $action_name ? $action_name : 'update';
            } elseif ($method === 'DELETE') {
                $action = $action_name ? $action_name : 'delete';
            } else {
                throw new Exception("Metodo [$method] desconocido!", 500);
            }
            self::$action_name = $action_name;
            //-------- ACCIONES POR DEFECTO -------------

            // sanear nombre de accion: nombre-accion -> NombreAccion
            $action = str_replace(' ','',ucwords(str_replace('-',' ',$action)));
        
            // funcion del controlador a ejecutar
            $action = 'action' . ucfirst($action); // ej. actionView;
            
            // conexion de base de datos
            $databases = [];
            foreach ($config['databases'] as $key => $value) {                
                $databases[$key] = new PDO($value['connection'], $value['db_user'], $value['db_pass']);
                @$databases[$key]->exec("SET NAMES '{$value['charset']}'");
            }
            MVC::$databases = $databases;
            MVC::$config = $config;
        
            // ejecucion de accion en el controlador
            if (!file_exists($controller_file)) {
                throw new Exception("Pagina no existe!", 404);
            }

            self::$controller_name = $controller_name;
            self::$controller_file = str_replace($base,'',$controller_file);
            require_once $controller_file;
            $controller = new $controller_name;
        
            // correr funcion del controlador
            if (!method_exists($controller,$action)) {
                throw new Exception("Metodo no existe!", 404);
            }
        } catch (Exception $e) {
            // error por excepcion
            error_log("{$e->getFile()}:{$e->getLine()} {$e->getMessage()}");
            MVC::http_error($e->getCode() < 400 ? 405 : $e->getCode(), $e->getMessage());
        } catch (Throwable $e) {
            // error mayor
            error_log("{$e->getFile()}:{$e->getLine()} {$e->getMessage()}");
            MVC::http_error($e->getCode() < 500 ? 500 : $e->getCode(), $e->getMessage());
        } finally {
            $controller->$action($id);
        }
        
    }

    function command($argv)
    {
        try {
            $config = require(__DIR__ . '/../config/main.php');
            $base = self::base_path();
            $arr_url = explode('/', $argv[1]);
            $controller_url = @array_shift($arr_url);
    
            # carpeta del controlador
            $controller_path = "{$base}/app";
            while ($controller_url and is_dir("{$controller_path}/modules/{$controller_url}")) {
                $controller_path .= "/modules/{$controller_url}";
                $controller_url = @array_shift($arr_url);
            }
    
            // sanear nombre de controlador: nombre-controlador -> NombreControlador
            $controller_name = str_replace(' ','',ucwords(str_replace('-',' ',$controller_url)));
            $controller_name = $controller_name ? $controller_name : 'site';
            $controller_name = ucfirst($controller_name . 'Controller');
    
            $controller_file = $controller_path . "/controllers/{$controller_name}.php";

            $action_name = array_shift($arr_url);
            $id = null;
            if (is_numeric($action_name)) {
                $id = $action_name;
                $action_name = null;
            } elseif ($action_name === 'id') {
                $action_name = null;
            }
        
            if ($id === null) {
                $id = array_shift($arr_url);
                if ($id === 'id') {
                    $id = array_shift($arr_url);
                }
            }

            if (!$action_name) $action_name = 'index';

            // sanear nombre de accion: nombre-accion -> NombreAccion
            $action = str_replace(' ','',ucwords(str_replace('-',' ',$action_name)));
        
            // funcion del controlador a ejecutar
            $action = 'action' . ucfirst($action); // ej. actionView;
            
            // conexion de base de datos
            $databases = [];
            foreach ($config['databases'] as $key => $value) {                
                $databases[$key] = new PDO($value['connection'], $value['db_user'], $value['db_pass']);
                @$databases[$key]->exec("SET NAMES '{$value['charset']}'");
            }
            MVC::$databases = $databases;
            MVC::$config = $config;

            // ejecucion de accion en el controlador
            if (!file_exists($controller_file)) {
                throw new Exception("Controlador no existe!", 404);
            }

            self::$controller_name = $controller_name;
            self::$controller_file = str_replace($base,'',$controller_file);

            require_once $controller_file;
            $controller = new $controller_name;
        
            // correr funcion del controlador
            if (!method_exists($controller,$action)) {
                throw new Exception("Metodo no existe!", 404);
            }

            // rescatar variables desde argumentos
            for ($i = 2; $i < count($argv); ++$i) { 
                list($key,$value) = explode('=',ltrim($argv[$i],'-'));
                $_GET[$key] = $value;
            }

        } catch (Exception $e) {
            // error por excepcion
            error_log("{$e->getFile()}:{$e->getLine()} {$e->getMessage()}");
            echo("{$e->getFile()}:{$e->getLine()} {$e->getMessage()}");
        } catch (Throwable $e) {
            // error mayor
            error_log("{$e->getFile()}:{$e->getLine()} {$e->getMessage()}");
            echo("{$e->getFile()}:{$e->getLine()} {$e->getMessage()}");
        } finally {
            $controller->$action($id);
        }
    }

    public function base_path()
    {
        return realpath(dirname(__FILE__) . '/..');
    }

    public function base_url()
    {
        return rtrim(str_replace('index.php','',$_SERVER['SCRIPT_NAME']),'/');
    }

    public function controller_file()
    {
        return self::$controller_file;
    }

    public function http_method()
    {
        return self::$http_method;
    }

    public function controller()
    {
        return self::$controller_name;
    }

    public function action()
    {
        return self::$action_name;
    }

    public function database($name)
    {
        return self::$databases[$name];
    }

    public function config($name = null)
    {
        return $name ? self::$config[$name] : self::$config;
    }

    /**
     * Muestra mensaje de error http con codigo de estado
     * @param integer $status estado del error
     * @param string $message mensaje de error
     * @return void
     */
    public function http_error($status = 500, $message = 'Ha ocurrido un error')
    {
        global $config;
        $config['debug'] && error_log($message);
        http_response_code($status);
        echo $message;
        exit($status);
    }

    /**
     * Retorna la información en formato JSON con presentación no comprimida.
     * @return string
     */
    public function json_pretty($data)
    {
        return json_encode($data, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK | JSON_BIGINT_AS_STRING | JSON_PARTIAL_OUTPUT_ON_ERROR);
    }
}