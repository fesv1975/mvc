<?php

/***
 * @author Francisco Sandoval V. <fesv1975@gmail.com>
 * @version 1.0.0
 * @created 2020
 *      _____                     _
 *     |  ___| __ __ _ _ __   ___(_)___  ___ ___
 *     | |_ | '__/ _` | '_ \ / __| / __|/ __/ _ \
 *     |  _|| | | (_| | | | | (__| \__ \ (_| (_) |
 *     |_|  |_|  \__,_|_| |_|\___|_|___/\___\___/
 *      ____                  _                 _  __     __
 *     / ___|  __ _ _ __   __| | _____   ____ _| | \ \   / /
 *     \___ \ / _` | '_ \ / _` |/ _ \ \ / / _` | |  \ \ / /
 *      ___) | (_| | | | | (_| | (_) \ V / (_| | |   \ V /
 *     |____/ \__,_|_| |_|\__,_|\___/ \_/ \__,_|_|    \_(_)
 *
 */

class Controller
{
    protected $model_name = null;
    protected $middleware_name = 'middleware_app';
    protected $layout = 'layouts/main';
    protected $model = null;
    private $request = null;

    public function __construct()
    {
        if ($this->middleware_name) {
            require MVC::base_path() . "/config/{$this->middleware_name}.php";
        }
        if ($this->model_name) {
            $this->model = new $this->model_name;
        }
    }

    /**
     * retorna al cliente listado de registros en formato json
     * en base al criterio de busqueda enviado
     *
     * @return void
     */
    public function actionIndex()
    {
        $this->json($this->model->getArray($this->request()));
    }

    /**
     * retorna al cliente la cantidad de registros en base al criterio enviado
     *
     * @return void
     */
    public function actionCount()
    {
        $this->json($this->model->count($this->request()));
    }

    /**
     * retorna al cliente los datos del registros en formato json
     * en base al id del registro
     *
     * @param string $id
     * @return void
     */
    public function actionView($id)
    {
        $this->json($this->model->getOne($id));
    }

    /**
     * crea un nuevo registro y retorna al cliente los datos en formato json
     * requiere que los datos hallan sido enviados con metodo POST
     *
     * @return void
     */
    public function actionCreate()
    {
        $data = $this->request();
        $result = $this->model->create($data);
        if ($id = $this->model->db()->lastInsertId()) {
            $this->actionView($id);
        } else {
            $this->json($data);
        }
    }

    /**
     * actualiza un registro y retorna al cliente los datos en formato json
     * en base al id del registro
     * requiere que los datos hallan sido enviados con metodo PUT
     *
     * @param string $id
     * @return void
     */
    public function actionUpdate($id)
    {
        $this->model->update($this->request(), $id);
        $this->actionView($id);
    }

    /**
     * crea o actualiza un registro, retorna los datos en formato json
     * requiere que los datos hallan sido enviados con metodo POST
     *
     * @return void
     */
    public function actionSave()
    {
        $data = $this->request();
        $pkey = $this->model->pkey();
        if ($id === null) $id = $data[$pkey];
        $model = $this->model->getOne($id);
        $model ? $this->actionUpdate($id) : $this->actionCreate();
    }

    /**
     * elimina un registro en base al id
     * retorna la cantidad de registros afectados
     *
     * @param string $id
     * @return void
     */
    public function actionDelete($id)
    {
        $this->json($this->model->delete($id));
    }

    /**
     * exporta los registros en formato csv, separados por punto y coma (;)
     * en base al criterio enviado
     *
     * @return void
     */
    public function actionExport()
    {
        $table_name = $this->model->table();
        $file_export = $table_name;
        if (substr(strtoupper($table_name), 0, 7) === '(SELECT') {
            $file_export = 'export';
        }

        $request = $this->request();
        $sql_order = isset($request['_order']) ? "ORDER BY {$request['_order']}" : '';
        $sql_limit = isset($request['_limit']) ? "LIMIT {$request['_limit']}" : '';
        $sql_offset = isset($request['_offset']) ? "OFFSET {$request['_offset']}" : '';
        $this->model->where($request,$sql_where,$params);
        
        $rs = $this->model->execute("SELECT * FROM {$table_name} WHERE {$sql_where} {$sql_order} {$sql_limit} {$sql_offset}", $params);

        $fs = fopen('php://temp', 'w+');
        for ($i = 0; $row = $rs->fetch(PDO::FETCH_ASSOC); ++$i) {
            if ($i === 0) {
                fputcsv($fs, array_keys($row), ';');
            }
            mb_convert_variables('Windows-1252', 'UTF-8', $row);
            fputcsv($fs, $row, ';');
        }
        rewind($fs);

        header('Content-Type: text/plain');
        header('Content-Disposition: attachment; filename="' . $file_export . '.csv"');
        echo stream_get_contents($fs);

        fclose($fs);
    }

    /**
     * importa la informacion enviada en formato csv separados por punto y coma (;)
     * se basa en el id del registro para crear o actualizar la informacion
     * retorna al cliente la cantidad de registros creados y actualizados en formato json
     *
     * @return void
     */
    public function actionImport()
    {
        if (!isset($_FILES['file1'])) {
            throw new Exception("No se ha enviado archivo csv " . print_r($_FILES, true), 1);
        }
        if ($_FILES['file1']['error'] !== 0) {
            throw new Exception("Error en archivo enviado", 1);
        }
        if (strtolower(substr($_FILES['file1']['name'], -4)) !== '.csv') {
            throw new Exception("Archivo no tiene extension csv", 1);
        }
        $fs = fopen($_FILES['file1']['tmp_name'], 'r');
        $result = [
            'created' => 0,
            'updated' => 0,
        ];
        $pkey = $this->model->pkey();
        for ($i = 0; $row = fgetcsv($fs, 0, ';'); ++$i) {
            if ($i === 0) {
                $keys = $row;
            } else {
                $data = array_combine($keys, $row);
                mb_convert_variables('UTF-8', 'Windows-1252', $data);
                if (trim($data[$pkey]) > '') {
                    $record = $this->model->getOne([$pkey => $data[$pkey]]);
                    if ($record) {
                        $data = array_merge((array) $producto, $data);
                        if ($this->model->update($data, [$pkey => $data[$pkey]])) {
                            $result['updated']++;
                        }
                    } else {
                        if ($this->model->create($data)) {
                            $result['created']++;
                        }
                    }
                }
            }
        }
        $this->json($result);
    }

    /**
     * imprime la data en formato json con la cabecera requerida
     *
     * @param array|object $data
     * @return void
     */
    public function json($data)
    {
        $fs = fopen('php://temp', 'r+');
        fwrite($fs, MVC::json_pretty($data));
        rewind($fs);

        header("Content-Type: application/json");
        header('Content-Encoding: gzip');

        ob_start('ob_gzhandler');
        echo stream_get_contents($fs);
        fclose($fs);
    }

    public function render($view, $data = [])
    {
        @list($controller, $path) = explode('/', $view);
        if ($path == '') {
            $path = $controller;
            $controller = strtolower(str_replace('Controller', '', get_called_class()));
        }
        $base = MVC::base_path();

        $view_file = dirname(dirname(MVC::controller_file())) . "/views/{$controller}/{$view}.php";
        $layout_file = dirname(dirname(MVC::controller_file())) . "/views/{$this->layout}.php";
        if (!is_file("{$base}{$layout_file}")) {
            $layout_file = "/app/views/{$this->layout}.php";
        }
        
        if (!is_file("{$base}{$view_file}")) {
            throw new Exception("No existe la vista [{$view_file}]", 500);
        }
        if (!is_file("{$base}{$layout_file}")) {
            throw new Exception("No existe la vista [{$layout_file}]", 500);
        }
        
        ob_start();
        extract($data);
        require $base . $view_file;
        $content = ob_get_clean();
        require $base . $layout_file;
    }

    public function renderRaw($view, $data = [])
    {
        @list($controller, $path) = explode('/', $view);
        if ($path == '') {
            $path = $controller;
            $controller = strtolower(str_replace('Controller', '', get_called_class()));
        }
        $base = MVC::base_path();

        $view_file = dirname(dirname(MVC::controller_file())) . "/views/{$controller}/{$view}.php";
        $layout_file = dirname(dirname(MVC::controller_file())) . "/views/{$this->layout}.php";
        if (!is_file("{$base}{$layout_file}")) {
            $layout_file = "/app/views/{$this->layout}.php";
        }
        
        if (!is_file("{$base}{$view_file}")) {
            throw new Exception("No existe la vista [{$view_file}]", 500);
        }
        if (!is_file("{$base}{$layout_file}")) {
            throw new Exception("No existe la vista [{$layout_file}]", 500);
        }
        
        extract($data);
        require $base . $view_file;
    }

    public function request($name = null)
    {
        if (!$this->request) {
            $input = json_decode(file_get_contents('php://input'), true);
            if (!$input) $input = [];
            $method = @$_SERVER['REQUEST_METHOD'];
            $request = (!$method or $method == 'GET') ? $_GET : $_POST;
            $this->request = array_merge($input, $request);
        }
        return $name ? $this->request[$name] : $this->request;
    }
}
