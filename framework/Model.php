<?php

/***
 * @author Francisco Sandoval V. <fesv1975@gmail.com>
 * @version 1.0.0
 * @created 2020
 *      _____                     _                         
 *     |  ___| __ __ _ _ __   ___(_)___  ___ ___            
 *     | |_ | '__/ _` | '_ \ / __| / __|/ __/ _ \           
 *     |  _|| | | (_| | | | | (__| \__ \ (_| (_) |          
 *     |_|  |_|  \__,_|_| |_|\___|_|___/\___\___/           
 *      ____                  _                 _  __     __
 *     / ___|  __ _ _ __   __| | _____   ____ _| | \ \   / /
 *     \___ \ / _` | '_ \ / _` |/ _ \ \ / / _` | |  \ \ / / 
 *      ___) | (_| | | | | (_| | (_) \ V / (_| | |   \ V /  
 *     |____/ \__,_|_| |_|\__,_|\___/ \_/ \__,_|_|    \_(_) 
 *                                                          
 */

class Model
{
    protected $dbname = null;
    protected $table = 'TABLE';
    protected $pkey = 'id';
    protected $columns = '*';
    protected $filter_search = null;
    private $db = null;
    private static $model = null;

    /**
     * seteo de inicio de clase
     *
     * @param PDO $db base de datos a usarse
     * @param string $table nombre de la tabla en la base de datos
     */
    public function __construct()
    {
        $class = get_class($this);
        $this->db = MVC::database($this->dbname);
    }

    static function model()
    {
        $class = get_called_class();
        return new $class;
    }

    public function db()
    {
        return $this->db;
    }

    public function table()
    {
        return $this->table;
    }

    public function pkey()
    {
        return $this->pkey;
    }

    /**
     * retorna la cantidad de registros en base a un criterio entregado
     *
     * @param array $where criterior de busqueda
     * @return integer
     */
    public function count($where = [])
    {
        if (!$where) {
            $where = [];
        }
        $this->where($where, $sql_where, $params);

        $sql = rtrim("SELECT count(*) FROM {$this->table} WHERE {$sql_where}");
        if ($query = $this->db->prepare($sql)) {
            if ($query->execute($params)) {
                return $query->fetchColumn();
            } else {
                error_log("SQL: {$sql} / " . print_r($params, true));
                throw new Exception('EXECUTE: ' . $query->errorInfo()[2], 1);
            }
        } else {
            error_log("SQL: {$sql} / " . print_r($params, true));
            throw new Exception('PREPARE: ' . $this->db->errorInfo()[2], 1);
        }
    }

    /**
     * busca los registros en base a un criterio entregado
     * retorna un array con el resultado
     *
     * @param array $where criterio de busqueda
     * @param string $order|null orden con que se entrega el resultado
     * @param integer $limit||0 cantidad maxima de registros a entregar
     * @param integer $offset|0 inicio de indice en entrega de resultado
     * @return array
     */
    public function getArray($where = [])
    {
        $sql_order = isset($where['_order']) ? "ORDER BY {$where['_order']}" : '';
        $sql_limit = isset($where['_limit']) ? "LIMIT {$where['_limit']}" : '';
        $sql_offset = isset($where['_offset']) ? "OFFSET {$where['_offset']}" : '';
        $this->where($where, $sql_where, $params);

        $sql = rtrim("SELECT {$this->columns} FROM {$this->table} WHERE {$sql_where} {$sql_order} {$sql_limit} {$sql_offset}");

        if ($query = $this->db->prepare($sql)) {
            if ($query->execute($params)) {
                // return $query;
                return $query->fetchAll(PDO::FETCH_CLASS);
            } else {
                error_log("SQL: {$sql} / " . print_r($params, true));
                throw new Exception('EXECUTE: ' . $query->errorInfo()[2], 1);
            }
        } else {
            error_log("SQL: {$sql} / " . print_r($params, true));
            throw new Exception('PREPARE: ' . $this->db->errorInfo()[2], 1);
        }
    }

    /**
     * busca un registro en base al criterio entregado
     * retorna el primer registro encontrado o null
     *
     * @param array $where criterio de busqueda
     * @return object PDO::OBJECT
     */
    public function getOne($where = null)
    {
        $sql_order = isset($where['_order']) ? "ORDER BY {$where['_order']}" : '';
        $sql_limit = isset($where['_limit']) ? "LIMIT {$where['_limit']}" : '';
        $sql_offset = isset($where['_offset']) ? "OFFSET {$where['_offset']}" : '';
        if (!is_array($where)) {
            $where = $where ? [ $this->pkey() => $where ] : [];
        }
        $this->where($where, $sql_where, $params);
        $sql = rtrim("SELECT * FROM {$this->table} WHERE {$sql_where} {$sql_order} {$sql_limit} {$sql_offset}");

        if ($query = $this->db->prepare($sql)) {
            if ($query->execute($params)) {
                $row = $query->fetchObject();
                return $row ? $row : null;
            } else {
                error_log("SQL: {$sql} / " . print_r($params, true));
                throw new Exception('EXECUTE: ' . $query->errorInfo()[2], 1);
            }
        } else {
            error_log("SQL: {$sql} / " . print_r($params, true));
            throw new Exception('PREPARE: ' . $this->db->errorInfo()[2], 1);
        }
    }

    /**
     * crea un nuevo registro
     * retorna 1 si es existoso
     *
     * @param array $data array de campo/valor
     * @return integer
     */
    public function create($data)
    {
        if (!count($data)) {
            throw new Exception("No se han indicado valores", 1);
        }
        $fields = [];
        $values = [];
        foreach ($data as $key => $value) {
            if (substr($key,0,1) === '_') {
                continue;
            }
            $fields[] = $key;
            $values[] = ":{$key}";
        }
        $sql = "INSERT INTO {$this->table} (" . implode(',', $fields) . ") VALUES (" . implode(',', $values) . ")";

        if ($query = $this->db->prepare($sql)) {
            if ($query->execute($data)) {
                return $query->rowCount();
            } else {
                error_log("SQL: {$sql} / " . print_r($data, true));
                throw new Exception('EXECUTE: ' . $query->errorInfo()[2], 1);
            }
        } else {
            error_log("SQL: {$sql} / " . print_r($data, true));
            throw new Exception('PREPARE: ' . $this->db->errorInfo()[2], 1);
        }
    }

    /**
     * actualiza registros con la data entregada en base al criterio entregado
     * retorna la cantidad de registros afectados
     *
     * @param array $data array de campo/valor a actualizar
     * @param array $where criterio de registros a actualizar
     * @return integer
     */
    public function update($data, $where = [])
    {
        if (!is_array($where)) {
            $where = $where ? [ $this->pkey() => $where ] : [];
        }
        if (!count($data)) {
            throw new Exception("No se han indicado valores", 1);
        }

        $sql_set = [];
        $params_set = [];
        foreach ($data as $key => $value) {
            if (substr($key,0,1) === '_') {
                continue;
            }
            $sql_set[] = "{$key} = :set_{$key}";
            $params_set["set_" . $key] = $value;
        }

        $this->where($where, $sql_where, $params_where);
        $sql = "UPDATE {$this->table} SET " . implode(', ', $sql_set) . " WHERE {$sql_where}";
        $data = array_merge($params_set, $params_where);

        if ($query = $this->db->prepare($sql)) {
            if ($query->execute($data)) {
                return $query->rowCount();
            } else {
                error_log("SQL: {$sql} / " . print_r($data, true));
                throw new Exception('EXECUTE: ' . $query->errorInfo()[2], 1);
            }
        } else {
            error_log("SQL: {$sql} / " . print_r($data, true));
            throw new Exception('PREPARE: ' . $this->db->errorInfo()[2], 1);
        }
    }

    /**
     * elimina registros del model en base al criterio entregado.
     * retorna la cantidad de registros afectados
     *
     * @param array $where criterio de filtro
     * @return integer
     */
    public function delete($where = [])
    {
        if (!is_array($where)) {
            $where = $where ? [ $this->pkey() => $where ] : [];
        }
        $this->where($where, $sql_where, $params);
        $sql = "DELETE FROM {$this->table} WHERE {$sql_where}";
        
        if ($query = $this->db->prepare($sql)) {
            if ($query->execute($params)) {
                return $query->rowCount();
            } else {
                error_log("SQL: {$sql} / " . print_r($params, true));
                throw new Exception('EXECUTE: ' . $query->errorInfo()[2], 1);
            }
        } else {
            error_log("SQL: {$sql} / " . print_r($params, true));
            throw new Exception('PREPARE: ' . $this->db->errorInfo()[2], 1);
        }
    }

    /**
     * Ejecuta una instruccion sql y retorna un array de resultados
     *
     * @param string $sql instruccion SQL
     * @param array $params parametros que pudiera tener la instruccion SQL
     * @return array PDO::FETCH_CLASS
     */
    public function query($sql, $params = [])
    {
        if ($query = $this->db->prepare($sql)) {
            if ($query->execute($params)) {
                return $query->fetchAll(PDO::FETCH_CLASS);
            } else {
                error_log("SQL: {$sql} / " . print_r($params, true));
                throw new Exception('EXECUTE: ' . $query->errorInfo()[2], 1);
            }
        } else {
            error_log("SQL: {$sql} / " . print_r($params, true));
            throw new Exception('PREPARE: ' . $this->db->errorInfo()[2], 1);
        }
    }

    /**
     * Ejecuta una instruccion sql y retorna un objeto de resultados
     *
     * @param string $sql instruccion SQL
     * @param array $params parametros que pudiera tener la instruccion SQL
     * @return PDOStatement
     */
    public function execute($sql, $params = [])
    {
        if ($query = $this->db->prepare($sql)) {
            if ($query->execute($params)) {
                return $query;
            } else {
                error_log("SQL: {$sql} / " . print_r($params, true));
                throw new Exception('EXECUTE: ' . $query->errorInfo()[2], 1);
            }
        } else {
            error_log("SQL: {$sql} / " . print_r($params, true));
            throw new Exception('PREPARE: ' . $this->db->errorInfo()[2], 1);
        }
    }


    public function lastId($sequence_name = null)
    {
        return $this->db->lastInsertId($sequence_name);
    }

    public function where($request, &$sql_where, &$params)
    {
        $sql_where = isset($request['_where']) ? "({$request['_where']})" : '1=1';
        $params_where = isset($request['_params']) ? $request['_params'] : [];
        $params = [];

        if (is_array($request)) {
            foreach ($request as $key => $value) {
                if (substr($key,0,1) === '_') {
                    continue;
                }
                if ($key === 'search' and $this->filter_search) {
                    if ($value > '') {
                        $params[$key] = '%' . str_replace(' ', '%', $value) . '%';
                        $sql_where .= " AND ({$this->filter_search})";
                    }
                } else {
                    $params[$key] = $value;
                    $sql_where .= " AND {$key} = :{$key}";
                }
            }
        }

        $params = array_merge($params,$params_where);
    }
}
