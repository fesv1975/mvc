<?php
require_once 'Model.php';
require_once 'Controller.php';
require_once 'MVC.php';

$file_class = dirname(__FILE__) . "/../vendor/autoload.php";
if (is_file($file_class)) {
    require $file_class;
}

spl_autoload_register(function ($nombre_clase) {
    // buscar class en carpeta models
    $base = MVC::base_path();
    $class_file = dirname(dirname(MVC::controller_file())) . "/models/{$nombre_clase}.php";
    if (!is_file("{$base}{$class_file}")) {
        $class_file = "/app/models/{$nombre_clase}.php";
    }

    if (is_file("{$base}{$class_file}")) {
        require $base . $class_file;
        return;
    }
});