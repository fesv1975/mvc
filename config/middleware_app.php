<?php
if (MVC::action() === 'create' and MVC::http_method() !== 'POST') {
    MVC::http_error(405,'Para crear un registro requiere metodo POST');
}
if (MVC::action() === 'update' and MVC::http_method() !== 'PUT') {
    MVC::http_error(405,'Para modificar un registro requiere metodo PUT');
}
if (MVC::action() === 'save' and MVC::http_method() !== 'POST') {
    MVC::http_error(405,'Para crear/modificar un registro requiere metodo POST');
}
if (MVC::action() === 'delete' and MVC::http_method() !== 'DELETE') {
    MVC::http_error(405,'Para eliminar un registro requiere metodo DELETE');
}
if ($this->request('_where') > '') {
    MVC::http_error(405,'Por seguridad no debe usarse (_where=...) en la url.');
}