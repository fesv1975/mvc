FRAMEWORK MVC.
==============

![Foto](/public/images/foto.png)   
Versión: 2.0   
Autor: Francisco Sandoval V.   
Fecha: Enero 2021

---

## Documentación.

### Estructura de carpetas.

**/index.php**   
  script inicial del framework, hace las llamadas al autoload del framework.

**/config/main.php**   
  Se ingresan los parametros de conexión a las base de datos que usará.

**/config/middleware_app.php**   
  Se ingresan las instrucciones iniciales antes de llegar al metodo a ejecutar debido a la ruta ingresada en el navegador.

**/framework/autoload.php**   
  Carga automaticamente los models a utilizar en cualquier controlador.
  Carga las clases de la carpeta */vendor* si existiera.

**/framework/Controller.php**   
  Controlador principal, este contiene metodos *crud* definidos por defectos los cuales pueden ser sobreescribidos en el controlador que lo hereda.

**/framework/Model.php**   
  Model principal, este contiene los metodos basicos para facilitar las instrucciones *crud* vía *sql*.

**/framework/MVC.php**   
  Traduce la ruta indicada en el navegador y lo dirige al controlador y metodo respectivo.
~~~
ruta       : http://server/compania-cervecera/actualiza-cliente/23
controlador: /app/controllers/CompaniaCerveceraController.php
metodo     : actionActualizaCliente($id) { ... }

ruta       : http://server/modulo-seguridad/permiso-usuario/asignaciones
controlador: /app/modules/modulo-seguridad/controllers/PermisoUsuarioController.php
metodo     : actionAsignaciones() { ... }

ruta       : http://server/modulo-seguridad/perfiles/mantencion/admin
controlador: /app/modules/modulo-seguridad/modules/perfiles/controllers/MantencionController.php
metodo     : actionAdmin() { ... }
~~~

**/app/views/layouts/main.php**   
  Se define la maqueta principal cuando se renderiza una página.

**/app/views/site/index.php**   
  Se define el fragmento de la página que se mostrará en la página.

**/app/controllers**   
  En esta carpeta se agregan los controladores personalizados, por defecto esta *SiteController.php*.

**/app/models**   
  En esta carpeta se agregan los models personalizados.

**/app/views**   
  En esta carpeta se agregan las vistas personalizadas.

**/app/modules**   
  En esta carpeta se agregan los modulos personalizados, debe mantener la misma estructura de carpetas que la carpeta */app*.   
~~~   
Ejemplo:
/app/modules/modulo-uno/controllers/   
/app/modules/modulo-uno/models/   
/app/modules/modulo-uno/views/   
/app/modules/modulo-uno/modules/    (si hay submodulos)   
~~~

---

### Funciones comunes.

**MVC::base_path()**   
  Retorna la carpeta base del sitio.

**MVC::base_url()**   
  Retorna la ruta url base del sitio.

**MVC::json_pretty(mixed \$data)**   
  Retorna la información en formato *JSON* con presentación no comprimida.

**MVC::http_error(int \$status, string \$message)**   
  Genera un error http para el cliente con un estado y un mensaje.

**MVC::database(string \$name)**   
  Retorna la conexión de base de datos.

**MVC::config()**   
  Retorna array con la configuración del framework.

**MVC::config(string \$name)**   
  Retorna valor de configuracion de un parametro.

**MVC::http_method()**   
  Retorna el metodo usado por el cliente (GET, POST, PUT, DELETE).

**MVC::controller()**   
  Retorna el nombre del controller llamado por la url.

**MVC::action()**   
  Retorna el nombre del metodo llamado por la url.


### Funciones en el controlador.
 
**\$this->request()**   
  Retorna un array de las variables enviadas por el cliente mediante *GET, POST, PUT, DELETE*.

**\$this->request(string \$name)**   
  Retorna el valor de una de las variables enviadas por el cliente mediante *GET, POST, PUT, DELETE*.

**\$this->render(string \$view, [array \$data])**   
  Renderiza una página pasando un array de variables a la vista, la vista usará la maqueta definida en el controlador en la variable.
~~~php
protected $layout = "layouts/main";
~~~

**\$this->renderRaw(string \$view, [array \$data])**   
  Renderiza una página pasando un array de variables a la vista, esta NO utilizá la maqueta principal, es de ayuda cuando la vista genera una IMAGEN o una salida PDF.

**\$this->json(mixed \$data)**   
  Envía al cliente la información en formato *JSON*.

---

### Variables protegidas del model.

**protected \$dbname = "db";**   
  Nombre de la conexión de base de datos que utiliza para llegar a la tabla.

**protected \$table = "user";**   
  Nombre de la tabla en la base de datos.

**protected \$pkey = "id";**   
  Nombre del campo identificador de registro, por defecto usa *id*.

**protected \$columns = "\*";**   
  Listado de campos de la tabla que retornaran al cliente, por defecto es *.

**protected \$filter_search = "nombre like :search or apellido like :search";**   
  Condición *SQL* para realizar una busqueda al utilizar el argumento *search*.


### Variables protegidas del controlador.

**protected \$model_name = "User";**   
  Se define el nombre del *model* principal a utilizar en el controlador para los metodos por defectos definidos.

**protected \$middleware_name = "middleware_app";**   
  Se define el nombre del archivo con instrucciones que se ejecutarán primeramente en el controlador, éste se encuentra en la carpeta */config*.

**protected \$layout = "layouts/main";**   
  Se define la maqueta principal que se utilizará cuando se rendericen las páginas en controlador, éste se encuentra en la carpeta */views/layouts*.

**protected \$model = null;**   
  Es la variable con el objeto *Model*, se inicializa usando *\$model_name*.

---

### Funciones definos en el model.

**pkey()**   
  Retorna el campo identificador definido en el model, por defecto "id".

**table()**   
  Retorna el nombre de la tabla definido en el model, por defecto "id".

**db()**   
  Retorna la conexión *PDO* definido y que usará el model.

**getOne([array \$condition])**   
  Retorna registro *PDO* de acuerdo a la condición.

**getArray([array \$condition])**   
  Retorna array de registros *PDO* de acuerdo a la condición.

**count([array \$condition])**   
  Retorna cantidad de registros que cumple la condición.

**create([array \$key_value])**   
  Inserta un nuevo registro en la tabla.

**update(array \$key_value, array \$condition)**   
  Actualiza la información del registro de acuerdo a la condición.

**delete(array \$condition)**   
  Elimina los registros de acuerdo a la condición y retorna la cantidad afectada.

**query(string \$sql, array \$params)**   
  Retorna array de registros de la consulta *SQL*.

**execute(string \$sql, array \$params)**   
  Retorna objeto *PDOStatement* de la instrucción *SQL*.

**lastId(string \$sequence_name)**   
  Retorna la ultima id generado por insert de la tabla.

**where(array \$key_value, string &\$sql_where, array &\$params)**   
  Construye la condición *SQL*  y parametros a utilizar en las funciones del model.


### Metodos predefinidos en el controlador.

**actionIndex()**   
  Por defecto envía al cliente el listado de registros en formato *JSON*.   
  El resultado varía si se envian argumentos que se usarán para filtrar los registros.

**actionCount()**   
  Por defecto envía al cliente la cantidad de registros.   
  La cantidad varía si se envian argumentos que se usarán para filtrar los registros.

**actionView(string \$id)**   
  Por defecto envía al cliente el registro con el id=\$id en formato *JSON*.

**actionCreate()**   
  Por defecto crea un registro de acuerdo a los argumentos enviados mediante el metodo *POST*.

**actionUpdate(string \$id)**   
  Por defecto actualiza el registro con el id=\$id de acuerdo a los argumentos enviados mediante el metodo *PUT*.

**actionDelete(string \$id)**   
  Por defecto elimina el registro con el id=\$id mediante el metodo *DELETE*.

**actionExport()**   
  Por defecto genera una salida en formato *CSV* con los registros.   
  El resultado varía si se envian argumentos que se usarán para filtrar los registros.

**actionImport()**   
  Por defecto crea o actualiza los registros en base al archivo en formato *CSV* enviado en la variable *\$_FILES["file1"]*.

---

### Argumentos que se utilizán para filtrar los resultados.

**_where**   
  Condición *SQL*, por seguridad solo debe usarse al llamar al model.

**_offset**   
  Registro inicial, desde cero.

**_limit**   
  Cantidad de registros a retornar.

**search**   
  Filtra el resultado de acuerdo a la condición *SQL* definida en el *model*.

**nombre_del_campo**   
  Filtra el resultado por el valor del argumento.
~~~sql
--- Ejemplo sql
--- url: /controlador/metodo?field1=234&fields2=567
and field1 = 234 and field2 = 567
~~~

---

### htaccess

~~~apacheconf
<IfModule mod_rewrite.c>
  Options +FollowSymLinks
  RewriteEngine On

  RewriteCond %{REQUEST_URI} !-f
  RewriteCond %{REQUEST_URI} !-d
  RewriteRule ^(public)($|/) - [L,NC]
  RewriteCond $1 !^(index\.php|robots\.txt)
  RewriteRule ^(.*)$ index.php?_r=$1 [QSA,L]
</IfModule>
~~~

---



fin.
